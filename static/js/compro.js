
var imageNr = new Date().getTime(); // Serial number of current image
var finished = new Array(); // References to img objects which have finished downloading
var paused = false;
var srcView = "http://81.90.235.50"

var imageNrTemp = 0;

function countFPS()
{
	var fpsMsg = "";
	fpsMsg += "Frame per second: ";
	fpsMsg += imageNr - imageNrTemp;
	imageNrTemp = imageNr;
	document.getElementById("fps").innerHTML = fpsMsg;
	
	setTimeout("countFPS();", 1000);
}

function createImageLayer() {
  var img = new Image();
  img.style.position = "absolute";
  img.style.zIndex = -1;
  img.onload = imageOnload;
  img.style.width = "320px";
  img.style.height = "240px";
  img.onclick = imageOnclick;
  img.src = srcView+"/mjpegStreamer.cgi?n=" + (++imageNr);

  var webcam = document.getElementById("webcam");
  webcam.insertBefore(img, webcam.firstChild);
//  img.focus();
}

// Two layers are always present (except at the very beginning), to avoid flicker
function imageOnload() {
  this.style.zIndex = imageNr; // Image finished, bring to front!
  while (1 < finished.length) {
    var del = finished.shift(); // Delete old image(s) from document
    del.parentNode.removeChild(del);
  }
  finished.push(this);
  
  if(!paused)
  	createImageLayer();
}

function imageOnclick() { // Clicking on the image will pause the stream
	paused = !paused;
	if(!paused)
		createImageLayer();
}

function startDownload()
{
	setTimeout("createImageLayer()", 0);
	setTimeout("createImageLayer()", 100);
}

var left = false;
var right = false;
var up = false;
var bottom = false;
function restoreRight()
{
    document.getElementById("right").src = srcView+"/images/mobile_right.jpg";
}
function restoreLeft()
{
    document.getElementById("left").src = srcView+"/images/mobile_left.jpg";
}
function restoreUp()
{
    document.getElementById("up").src = srcView+"/images/mobile_up.jpg";
}
function restoreBottom()
{
    document.getElementById("bottom").src = srcView+"/images/mobile_bottom.jpg";
}
function restoreHome()
{
    document.getElementById("home").src = srcView+"/images/mobile_home.jpg";
}

function restoreZoomin()
{
    document.getElementById("zoomin").src = srcView+"/images/mobile_zoom_in.jpg";
}
function restoreZoomout()
{
    document.getElementById("zoomout").src = srcView+"/images/mobile_zoom_out.jpg";
}
function restoreDo1()
{
    document.getElementById("do1").src = srcView+"/images/mobile_do1.jpg";
}
function restoreDo2()
{
    document.getElementById("do2").src = srcView+"/images/mobile_do2.jpg";
}
function restoreDo3()
{
    document.getElementById("do3").src = srcView+"/images/mobile_do3.jpg";
}

function hover(dir, obj)
{
    if (dir == "left")
    {
	obj.src = srcView+"/images/mobile_left_down.jpg";
	setTimeout("restoreLeft()", 500);
    }
    if (dir == "right")
    {
	obj.src = srcView+"/images/mobile_right_down.jpg";
	setTimeout("restoreRight()", 500);
    }
    if (dir == "up")
    {
	obj.src = srcView+"/images/mobile_up_down.jpg";
	setTimeout("restoreUp()", 500);
    }
    if (dir == "bottom")
    {
	obj.src = srcView+"/images/mobile_bottom_down.jpg";
	setTimeout("restoreBottom()", 500);
    }
    if (dir == "home")
    {
	obj.src = srcView+"/images/mobile_home_down.gif";
	setTimeout("restoreHome()", 500);
    }


    if (dir == "zoomin")
    {
	obj.src = srcView+"/images/mobile_zoom_in_down.jpg";
	setTimeout("restoreZoomin()", 500);
    }
    if (dir == "zoomout")
    {
	obj.src = srcView+"/images/mobile_zoom_out_down.jpg";
	setTimeout("restoreZoomout()", 500);
    }

    if (dir == "do1")
    {
	obj.src = srcView+"/images/mobile_do1_down.jpg";
	setTimeout("restoreDo1()", 500);
    }
    if (dir == "do2")
    {
	obj.src = srcView+"/images/mobile_do2_down.jpg";
	setTimeout("restoreDo2()", 500);
    }
    if (dir == "do3")
    {
	obj.src = srcView+"/images/mobile_do3_down.jpg";
	setTimeout("restoreDo3()", 500);
    }
}

function round(min, max)
{
    return Math.round(Math.random()*(max-min)+min);
}

function DigOutOn(dev)
{
    var newtime = new Date().getTime();
    window.frames['Message'].location.href = srcView+'/mjpeg_setting.cgi?device='+dev+'&rnd='+round(1000,223344);
}